//alert('ok');
 
//  objeto JSON com os dados
var dados = [
    {
      pilha: "Pilha Alcalina Duracel AA",
      tensao_nominal: 1.5,
      corrente: 2800,
      E: 1.304,
      V: 1.286,
      R: 23.7,
    },
    {
      pilha: "Bateria 12V 7Ah",
      tensao_nominal: 12,
      corrente: 7000,
      E: 10.48,
      V: 10.32,
      R: 23.7,
    },
    {
      pilha: "Pilha Alcalina Duracel AA",
      tensao_nominal: 1.5,
      corrente: 2800,
      E: 1.304,
      V: 1.286,
      R: 23.7,
    },
    {
      pilha: "Bateria 12V 7Ah",
      tensao_nominal: 12,
      corrente: 7000,
      E: 10.48,
      V: 10.32,
      R: 23.7,
    },
  ];
   
  // Função para calcular a resistencia interna de cada medição e preencher a tabela
  function calcularResistencia() {
    var tabela = document.getElementById("tbDados");
   
    // Limpar tabela antes de preencher
    var tabelaHTML = `<tr>
                              <th>Pilha/Bateria</th>
                              <th>Tensão nominal (V)</th>
                              <th>Capacidade de corrente (mA.h)</th>
                              <th>Tensão sem carga(V)</th>
                              <th>Tensão com carga(V)</th>
                              <th>Resitência de carga(ohm)</th>
                              <th>Resistência interna(ohm)</th>
                    </tr>`;
   
    // Iterar sobre os dados e calcular o r de cada um
    for (var i = 0; i < dados.length; i++) {
      var linha = dados[i];
      var E = linha.E;
      var V = linha.V;
      var R = linha.R;
      var r = R * (E / V - 1);
   
      // Adicionar nova linha à tabela com os valores e a soma
      var novaLinha = "<tr><td>" + linha.pilha + "</td><td>" + linha.tensao_nominal + "</td><td>" + linha.corrente + "</td><td>" + linha.E + "</td><td>" + linha.V + "</td><td>" + linha.R + "</td><td>" + r.toFixed(4) + "</td></tr>";
   
      tabelaHTML += novaLinha;
    }
    tabela.innerHTML = tabelaHTML;
  }
   
  calcularResistencia();
  