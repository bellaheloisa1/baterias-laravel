<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BateriasLaravel</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/estilos.css')}}">
    <style>

        body{
            background-color: rgb(131,111,255);
        }
    </style>
</head>

<body>
    <div class="container-md">
        <h1> Baterias Laravel</h1>
        <hr>
        <nav>
            <ul>
                <li><a href="{{ route('/') }}">Início</a></li>
                <li><a href="{{ route('teoria') }}">Teoria</a></li>
                <li><a href="{{ route('procedimento') }}">Procedimento</a></li>
                <li><a href="{{ route('medicoes') }}">Medições</a></li>
                <li><a href="{{ route('conclusoes') }}">Conclusões</a></li>
            </ul>
        </nav>
        @yield("conteudo")
        <hr>
        @yield("rodape")
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
</body>
</html>